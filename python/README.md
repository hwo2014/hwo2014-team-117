## Python bot template for HWO-2014

Install (OSX):

    brew install python
    pip install virtualenv

Install (Debian/Ubuntu):

    sudo apt-get install python-pip
    sudo pip install virtualenv


## TurtleX Notes

Questions:

Is it better to stay in the lowest lane?

* shorter way
* higher chance of crash at same velocity
* switch time

A greedy strategy might not pay off when
there are more than two lanes.

Encoding for turns, e.g. Keimola (2 lanes):

    RLLRLRLR

Convert the game init track information into a string.

Keimola would look like this fully encoded:

    IIIIXRRRRRXIILIIXLLLLIXRRRRRLIXRRILXLRRRRIXIIII

Two lane case:

Starting at the outer lane (0), manually finding the shortest path:

    IIIIXRRRRRXIILIIXLLLLIXRRRRRLIXRRILXLRRRRIXIIII
    0000/11111\00000000000/1111111|1111|111111|1111
    1111|11111\00000000000/1111111|1111|111111|1111
    1111|11111\00000000000/1111111|1111|111111|1111

Starting at the inner lane(1), the 1st row would look like the 2nd or 3rd.

If there are more `L` than `R` up to the next `X`, turn `Left`,
otherwise turn `Right`.




