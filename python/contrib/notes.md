# def get_car_position(data, team='TurtleX'):
#     for position_data in data:
#         if position_data.get('id').get('name') == 'TurtleX':
#             return position_data


# def encode_track(data):
#     """
#     Encode track information in a single string. Tested with a two lane track.
#     `I` = straight
#     `L` = left turn
#     `R` = right turn
#     `X` = switch
#     Input is a complete `gameInit` message from the server.
#     """
#     encoded = []
#     race = data.get('msg').get('data').get('race')
#     pieces = race.get('track').get('pieces')
#     for piece in pieces:
#         if 'length' in piece:
#             encoded.append('I')
#         elif 'angle' in piece:
#             angle = piece.get('angle')
#             if angle > 0:
#                 encoded.append('R')
#             elif angle < 0:
#                 encoded.append('L')
#             else:
#                 raise ValueError('0 degree angle?')
#         if 'switch' in piece:
#             encoded.append('X')        
#     return ''.join(encoded)


# def greedy_lane_switcher(encoded_track, start_lane=0, lanecount=2):
#     """ Visually display the greedy lane switch strategy.
#     Input is an `encoded_track` encoded track, e.g.:

#     IIIIXRRRRRXIILIIXLLLLIXRRRRRLIXRRILXLRRRRIXIIII

#     Output:

#     0000|00000\11111|11111/0000000|0000|000000|0000    
#     """
#     cyclic_track = encoded_track + encoded_track
#     greedy_lane = []
#     current_lane = start_lane
#     for i, piece in enumerate(encoded_track):
#         if piece in ('I', 'R', 'L'):
#             greedy_lane.append(current_lane)
#         elif piece == 'X':
#             left_turns, right_turns = 0, 0
#             for next in cyclic_track[i + 1:]:
#                 if next == 'R':
#                     right_turns += 1
#                 elif next == 'L':
#                     left_turns += 1
#                 elif next == 'X':
#                     break
#                 elif next == 'I':
#                     pass
#                 else:
#                     raise ValueError('unknown track segment: %s' % next)
#             if left_turns > right_turns:
#                 lane_to_be = min(current_lane + 1, lanecount - 1)
#                 if lane_to_be == current_lane:
#                     greedy_lane.append('^')
#                 else:
#                     greedy_lane.append('<')
#                 current_lane = lane_to_be
#             elif left_turns < right_turns:
#                 lane_to_be = max(current_lane - 1, 0)
#                 if lane_to_be == current_lane:
#                     greedy_lane.append('^')
#                 else:
#                     greedy_lane.append('>')
#                 current_lane = lane_to_be
#             else:
#                 greedy_lane.append('^')
#     return ''.join(map(str, greedy_lane))    
