#!/usr/bin/env python
# coding: utf-8

"""
Encode the track into a single string.


"""
import argparse
import json
import itertools

def encode_track(data):
    """ I straight, L left turn, R right turn, S switch
    """
    encoded = []
    race = data.get('msg').get('data').get('race')
    pieces = race.get('track').get('pieces')
    for piece in pieces:
        if 'length' in piece:
            encoded.append('I')
        elif 'angle' in piece:
            angle = piece.get('angle')
            if angle > 0:
                encoded.append('R')
            elif angle < 0:
                encoded.append('L')
            else:
                raise ValueError('0 degree angle?')
        if 'switch' in piece:
            encoded.append('X')        

    return ''.join(encoded)


def switch(encoded, start_lane=0, lanes=4):
    """ Given an encoded string race track, return the switches to use for
    shortest path. 

    Example input:

    IIIIXRRRRRXIILIIXLLLLIXRRRRRLIXRRILXLRRRRIXIIII

    Example output w starting lane 0 (lower):

    0000|00000\11111|11111/0000000|0000|000000|0000

    Greedy lane switcher without other cars.
    """
    cyclic = encoded + encoded
    lane = []
    current_lane = start_lane
    for i, c in enumerate(encoded):
        if c in ('I', 'R', 'L'):
            lane.append(current_lane)
        elif c == 'X':
            turn, lefts, rights = '|', 0, 0
            for next in cyclic[i + 1:]:
                if next == 'R':
                    rights += 1
                elif next == 'L':
                    lefts += 1
                elif next == 'X':
                    break
            if lefts > rights:
                next_lane = min(current_lane + 1, lanes - 1)
                if current_lane == next_lane:
                    turn = '|'
                else:
                    turn = '\\'
                current_lane = next_lane
            elif rights > lefts:
                next_lane = max(current_lane - 1, 0)
                if current_lane == next_lane:
                    turn = '|'
                else:
                    turn = '/'
                current_lane = next_lane
            lane.append(turn)
        else:
            raise ValueError('Invalid input.')
    return ''.join(map(str, lane))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, nargs=1)
    args = parser.parse_args()
    filename = args.filename[0]
    with open(filename) as handle:
        data = json.loads(handle.read())
        encoded = encode_track(data)
        print(encoded)
        switches = switch(encoded)
        print(switches)

