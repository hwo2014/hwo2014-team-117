# coding: utf-8
from __future__ import print_function
import json
import socket
import sys
import random
import abc

class LaneStrategy(object):
    """ An abstract lane switch strategy. """

    @abc.abstractmethod
    def lanes(self):
        """ return all lanes """
        pass

    @abc.abstractmethod
    def pieces(self):
        """ return all pieces """
        pass

    @abc.abstractmethod
    def switch(self, car_positions_data):
        """ Given all *carPositions*, calculate switch under this strategy.
        Returns None, 'Left' or 'Right'.
        """
        pass

class ThrottleStrategy(object):
    """ An abstract throttling strategy. """

    @abc.abstractmethod
    def throttle(self, car_positions_data):
        """ Given all *carPositions*, return the throttle value for this
        strategy.
        """
        pass

class RandomThrottleStrategy(ThrottleStrategy):
    """ Just set throttle to a random value. """

    def __init__(self, game_init_data=None, lower=0.5, upper=0.75):
        self.game_init_data = game_init_data
        self.lower = lower
        self.upper = upper

    def throttle(self, car_positions_data):
        return random.uniform(self.lower, self.upper)


class DumbLaneStrategy(LaneStrategy):
    """ Just switch to the lowest lane possible. """

    def __init__(self, game_init_data=None):
        self.game_init_data = game_init_data
        self.last_sent_on = None

    def lanes(self):
        if self.game_init_data:
            return self.game_init_data.get('race').get('track').get('lanes')
        return []

    @staticmethod
    def get_car_position(data, team='TurtleX'):
        for position_data in data:
            if position_data.get('id').get('name') == 'TurtleX':
                return position_data
        raise ValueError('unknown team: %s' % team)

    def pieces(self):
        if self.game_init_data:
            return self.game_init_data.get('race').get('track').get('pieces')
        return []

    def switch(self, car_positions_data):
        """ We do not need to know anything,
        because we only switch to the right. """
        car_position = self.get_car_position(car_positions_data)
        piece_index = car_position.get('piecePosition').get('pieceIndex')
        if self.last_sent_on == piece_index:
            # do no resend the command, if already sent
            return None
        entry = car_position.get('piecePosition').get('lane')
        eli = entry.get('end_lane_index')
        if eli > 0:
            self.last_sent_on = piece_index
            return "Right"
        return None


class SimpleBot(object):
    """ Simple Bot.

    Implements a greedy one-at-a-time lane switcher.
    A k-piece lookahead for throttling.
    """
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        # pluggable strategies
        self.switcher = DumbLaneStrategy()
        self.throttler = RandomThrottleStrategy(lower=0.5, upper=0.75)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined", file=sys.stderr)
        self.ping()

    def on_game_start(self, data):
        print("Race started", file=sys.stderr)
        print(json.dumps(data))

    def on_car_positions(self, data):
        """ Use the switching strategy to switch. """
        turn = self.switcher.switch(data)
        if turn:
            self.msg("switchLane", turn)
        
        throttle = self.throttler.throttle(data)
        self.msg("throttle", throttle)

    def on_crash(self, data):
        print("Someone crashed", file=sys.stderr)
        self.ping()

    def on_game_end(self, data):
        print("Race ended", file=sys.stderr)
        self.ping()

    def on_game_init(self, data):
        self.switcher.game_init_data = data
        self.throttler.game_init_data = data
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data), file=sys.stderr)
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            print(json.dumps(dict(msg_type=msg_type, msg=msg, data=data)))
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type), file=sys.stderr)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey", file=sys.stderr)
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:", file=sys.stderr)
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]), file=sys.stderr)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
