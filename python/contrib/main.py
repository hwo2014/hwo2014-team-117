from __future__ import print_function
import json
import socket
import sys
import random


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.lane_changed_right = 0

    def my_car_position(self, data):
        """ just return my car position. """
        for position_data in data:
            if position_data.get('id').get('name') == 'TurtleX':
                return position_data


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined", file=sys.stderr)
        self.ping()

    def on_game_start(self, data):
        print("Race started", file=sys.stderr)
        if data:
            print(json.dumps(data))

    def on_car_positions(self, data):
        if data:
            print(json.dumps(data))
        
        
        # linear vs curvy
        my = self.my_car_position(data)
        current_piece_index = my.get('piecePosition').get('pieceIndex')
        current_lap = my.get('piecePosition').get('lap')
        if current_piece_index in (34, 35, 36, 37, 38, 39):
            self.throttle(1.0)
        if current_piece_index in (8, 9, 10, 11):
            self.throttle(0.8)
        if current_piece_index in (0, 1, 2) and current_lap >= 1:
            throttle = random.uniform(0.2, 0.5)
            self.throttle(throttle)
        else:
            throttle = random.uniform(0.5, 0.75)
            # only send 3/4 of the messages
            if random.uniform(0, 1) > 0.25:
                self.throttle(throttle)
            # only switch lane once
            if self.lane_changed_right < 1:
                self.msg("switchLane", "Right")
            print(json.dumps(dict(throttle=throttle)))

    def on_crash(self, data):
        print("Someone crashed", file=sys.stderr)
        self.ping()

    def on_game_end(self, data):
        print("Race ended", file=sys.stderr)
        self.ping()

    def on_game_init(self, data):
        if data:
            print(json.dumps(data))
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data), file=sys.stderr)
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            print(json.dumps(dict(msg_type=msg_type, msg=msg, data=data)))
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type), file=sys.stderr)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey", file=sys.stderr)
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:", file=sys.stderr)
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]), file=sys.stderr)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
